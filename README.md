# to-be-continuous utils

Various to-be-continuous internal utility tools.

## template-to-component

This program helps migrating to-be-continuous legacy templates (include:project) to GitLab CI/CD components (include:component).

Usage:

```bash
# install dependencies
poetry install

# obtain help
poetry run t2c --help

# run tool
poetry run t2c path/to/tbc/template
```
