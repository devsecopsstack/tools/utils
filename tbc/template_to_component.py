import argparse
import json
from enum import Enum
from logging import Logger
import os
from pathlib import Path
import re
import shutil
from typing import Optional, Union

import yaml
from pydantic import BaseModel

LOGGER = Logger(__name__)


def str_presenter(dumper, data):
    """configures yaml for dumping multiline strings
    Ref: https://stackoverflow.com/questions/8640959/how-can-i-control-what-scalar-form-pyyaml-uses-for-my-data"""
    if data.count('\n') > 0:  # check for multiline string
        return dumper.represent_scalar('tag:yaml.org,2002:str', data, style='|')
    return dumper.represent_scalar('tag:yaml.org,2002:str', data)

yaml.add_representer(str, str_presenter)
yaml.representer.SafeRepresenter.add_representer(str, str_presenter) # to use with safe_dum


class GlInputType(str, Enum):
    """GitLab CI/CD component input type."""
    string = "string"
    boolean = "boolean"
    number = "number"


class TbcVarType(str, Enum):
    """to-be-continuous variable type."""
    text = "text"
    boolean = "boolean"
    number = "number"
    enum = "enum"
    url = "url"

    def to_gl(self) -> GlInputType:
        if self == TbcVarType.boolean:
            return GlInputType.boolean
        if self == TbcVarType.number:
            return GlInputType.number
        return GlInputType.string


_UNDEF = "!undef!"


class GlInput(BaseModel):
    name: str
    description: Optional[str] = None
    type: GlInputType = GlInputType.string
    options: Optional[list[str]] = None
    default: Optional[Union[str, bool, int]] = None

    def to_json(self) -> dict[str, any]:
        ret = {}
        if self.description:
            ret["description"] = self.description
        if self.type != GlInputType.string:
            ret["type"] = self.type.value
        if self.options:
            ret["options"] = self.options
        if self.default != _UNDEF:
            # TODO: ci-lint doesn't allow default: null (500 error)
            # ret["default"] = self.default
            ret["default"] = "" if self.default == None else self.default
        return ret


class TbcVar(BaseModel):
    name: str
    description: Optional[str] = None
    type: TbcVarType = TbcVarType.text
    values: Optional[list[str]] = None
    default: Optional[str] = None
    advanced: bool = False
    secret: bool = False
    mandatory: bool = False

    def to_gl(self, var_prefix: str) -> GlInput:
        return GlInput(
            name=(self.name[len(var_prefix):] if self.name.startswith(var_prefix) else self.name).replace('_', '-').lower(),
            # TODO: remove Markdown formatting?
            description=self.description,
            type=self.type.to_gl(),
            default=self.gl_dflt,
            options=self.values
        )
    
    @property
    def gl_dflt(self):
        if self.type == TbcVarType.boolean:
            return bool(self.default) if self.default != None else False
        if self.type == TbcVarType.number:
            return int(self.default) if self.default != None else 0
        return self.default


def _get_var(tpl_desc: dict[str, any], name: str) -> Optional[TbcVar]:
    var = next(iter([TbcVar.parse_obj(var) for var in tpl_desc.get("variables", []) if var["name"] == name]), None)
    if var:
        return var

    # look into feature variables
    for feat in tpl_desc.get("features", []):
        if feat.get("enable_with") == name:
            return TbcVar(
                name=feat.get("enable_with"),
                description= f"Enable {feat['name']}",
                type=TbcVarType.boolean,
            )
        elif feat.get("disable_with") == name:
            return TbcVar(
                name=feat.get("disable_with"),
                description= f"Disable {feat['name']}",
                type=TbcVarType.boolean,
            )
        var = next(iter([TbcVar.parse_obj(var) for var in feat.get("variables", []) if var["name"] == name]), None)
        if var:
            return var
    
    return None


def _migrate_var(var: TbcVar, var_prefix: str, tpl: dict[str, any], main_tpl: Optional[dict[str, any]]) -> Optional[GlInput]:
    if var.secret:
        # secrets should not be inputs
        print(f"INFO: variable '{var.name}' is a secret: skip")
        return None
    if main_tpl and _get_var(main_tpl, var.name):
        # a variant is overriding a variable from the main template: skip
        print(f"INFO: variable '{var.name}' is an override: skip")
        return None
    if var.name.startswith("TBC_"):
        # global TBC variable: skip
        print(f"INFO: variable '{var.name}' is global TBC: skip")
        return None
    
    gl_inputs = var.to_gl(var_prefix)
    inputs_binding = f"$[[ inputs.{gl_inputs.name} ]]"

    # check if default is defined and equals in template
    tpl_default = tpl["variables"].get(var.name)
    if isinstance(tpl_default, dict):
        # detailed variable form (value, options, description)
        tpl_default = tpl_default.get("value")
    if tpl_default and tpl_default != inputs_binding and var.default != tpl_default:
        print(f"WARN: variable '{var.name}' has different default values")
        print(f"| template: {tpl_default}")
        print(f"| kicker  : {var.default}")
        # apply template default instead
        var.default = tpl_default
        # update input
        gl_inputs = var.to_gl(var_prefix)

    # migrate variable default value with interpolation format
    tpl["variables"][var.name] = inputs_binding

    return gl_inputs


def _migrate_tpl(tpl_desc, main_tpl_desc, project_dir: Path, var_prefix: str) -> list[tuple[TbcVar, Optional[GlInput]]]:
    ret: list[tuple[TbcVar, Optional[GlInput]]] = []
    tpl_path = project_dir / tpl_desc['template_path']
    # load template
    with open(tpl_path, "r") as tpl_reader:
        all_tpl = list(yaml.load_all(tpl_reader, Loader=yaml.BaseLoader))
        tpl = all_tpl[-1]

    # migrate main template variables
    for var in tpl_desc.get("variables", []):
        tbc_var = TbcVar.parse_obj(var)
        ret.append((tbc_var, _migrate_var(tbc_var, var_prefix, tpl, main_tpl_desc)))

    # migrate feature variables
    for feat in tpl_desc.get("features", []):
        if feat.get("enable_with"):
            tbc_var = TbcVar(name=feat.get("enable_with"),
                description= f"Enable {feat['name']}",
                type=TbcVarType.boolean,
            )
            ret.append((tbc_var, _migrate_var(tbc_var, var_prefix, tpl, main_tpl_desc)))
        elif feat.get("disable_with"):
            tbc_var = TbcVar(name=feat.get("disable_with"),
                description= f"Disable {feat['name']}",
                type=TbcVarType.boolean,
            )
            ret.append((tbc_var, _migrate_var(tbc_var, var_prefix, tpl, main_tpl_desc)))
        for var in feat.get("variables", []):
            tbc_var = TbcVar.parse_obj(var)
            ret.append((tbc_var, _migrate_var(tbc_var, var_prefix, tpl, main_tpl_desc)))
    
    tpl_spec = {"spec": {"inputs": {inputs.name: inputs.to_json() for _, inputs in ret if inputs}}}

    # migrate template
    print(">> Migrate template")
    with open(tpl_path, "r") as tpl_reader:
        tpl_content = tpl_reader.read()

    # insert specs
    # print(">> Template spec:")
    # print(yaml.dump(tpl_spec, width=1000, default_flow_style=False, sort_keys=False))
    # print()
    first_non_comment_line: re.Match = next(re.finditer(r"^[^#]", tpl_content, flags=re.MULTILINE))
    tpl_content = tpl_content[0:first_non_comment_line.start(0)]+yaml.dump(tpl_spec, width=1000, default_flow_style=False, sort_keys=False)+"---\n"+tpl_content[first_non_comment_line.start(0):]

    # overwrite variables
    new_vars = dict()
    for key, val in tpl["variables"].items():
        var_decl_match = next(re.finditer(f"^  {key}: *(.*)$", tpl_content, flags=re.MULTILINE), None)
        if var_decl_match:
            # replace existing declaration
            tpl_content = tpl_content[0:var_decl_match.start(1)]+val+tpl_content[var_decl_match.end(1):]
        else:
            # add declaration (end of variables section)
            new_vars[key] = val
    
    if new_vars:
        variables_block: re.Match = next(re.finditer("^variables:", tpl_content, flags=re.MULTILINE))
        first_block_after_variables: re.Match = next(re.compile(r"^[a-zA-Z#0-9\.]", flags=re.MULTILINE).finditer(tpl_content, pos=variables_block.end(0)))
        # insert new vars here
        tpl_content = tpl_content[0:first_block_after_variables.start(0)]+("\n".join([f"  {key}: {val}" for key, val in new_vars.items()]))+"\n\n"+tpl_content[first_block_after_variables.start(0):]

    # write
    with open(tpl_path, "w") as writer:
        writer.write(tpl_content)

    # print(">> Variables sections:")
    # # print(yaml.dump({"variables": tpl["variables"]}, default_flow_style=False, sort_keys=False))
    # print(yaml.dump({"variables": new_vars}, default_flow_style=False, sort_keys=False))
    # print()

    return ret


def _migrate_readme(tpl_desc, project_dir: Path, var_input: list[tuple[TbcVar, Optional[GlInput]]]):
    # read file
    readme_file = project_dir / "README.md"
    with open(readme_file, "r") as reader:
        readme_content = reader.read()
    
    project_name = project_dir.name
    project_version = "TODO.VERSION"

    # insert new usage chapter
    readme_content = readme_content.replace("## Usage", f"""## Usage

This template can be used both as a [CI/CD component](https://docs.gitlab.com/ee/ci/components/#use-a-component-in-a-cicd-configuration) 
or using the legacy [`include:project`](https://docs.gitlab.com/ee/ci/yaml/index.html#includeproject) syntax.

### Use as a CI/CD component

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # 1: include the component
  - component: gitlab.com/to-be-continuous/{project_name}/{tpl_desc['template_path'].split('/')[-1].split('.')[0]}@{project_version}
    # 2: set/override component inputs
    inputs:
      example-input: "TODO" # ⚠ this is only an example
```

### Use as a CI/CD template (legacy)

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # 1: include the template
  - project: 'to-be-continuous/{project_name}'
    ref: '{project_version}'
    file: '/{tpl_desc['template_path']}'

variables:
  # 2: set/override template variables
  EXAMPLE_VAR: "TODO" # ⚠ this is only an example
```

## Usage (previous)""")
    
    # replace "Name" header in tables by "Input / Variable"
    readme_content = re.sub(r"\| *Name *\|", "| Input / Variable |", readme_content)
    # readme_content = re.sub(r"\| *description", "| Description", readme_content)
    # readme_content = re.sub(r"\| *default value", "| Default value", readme_content)

    # replace all occurrences "| SOME_VAR |" by "| some-var / SOME_VAR |"
    for var, input in var_input:
        if input:
            readme_content = re.sub(rf"\| *`{var.name}` *\|", f"| `{input.name}` / `{var.name}` |", readme_content)
    # write
    with open(readme_file, "w") as writer:
        writer.write(readme_content)


def migrate():
    parser = argparse.ArgumentParser(
                    prog='template-to-component',
                    description='This tool migrates a legacy TBC template to a new GitLab CI/CD component')
    parser.add_argument('project_dir')
    parser.add_argument('--var-prefix')

    args = parser.parse_args()
    project_dir = Path(args.project_dir)
    if not project_dir.exists():
        raise ValueError("<project-dir> must exist")
    if not project_dir.is_dir():
        raise ValueError("<project-dir> must be a directory")

    with open(project_dir / "kicker.json", "r") as kicker_file:
        kicker = json.load(kicker_file)
    
    # guess prefix
    var_prefix = (args.var_prefix or next(map(lambda var: var["name"].split("_")[0], filter(lambda var: var["name"].endswith("_IMAGE"), kicker.get("variables", []))), None) or kicker["name"].upper()) + "_"

    var_input: list[tuple[TbcVar, Optional[GlInput]]] = []

    print(f"=== Migrating main template '{kicker['name']}' with var prefix '{var_prefix}'...")
    print()

    var_input.extend(_migrate_tpl(kicker, None, project_dir, var_prefix))
    
    for variant in kicker.get("variants", []):
        print(f"=== Migrating variant '{variant['name']}' with var prefix '{var_prefix}'...")
        print()
        var_input.extend(_migrate_tpl(variant, kicker, project_dir, var_prefix))

    print("=== Migrating README...")
    print()
    _migrate_readme(kicker, project_dir, var_input)

    print("=== Copying bumpversion.sh...")
    print()
    shutil.copy(Path(__file__).parent / "bumpversion.sh", project_dir)
