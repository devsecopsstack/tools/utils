import io
import sys

import yaml

from tbc.template_to_component import _migrate_var


def test_migrate_var_boolean(self):
    input_var = {
            "name": "PYTEST_ENABLED",
            "description": f"Disable PYTEST job",
            "type": "boolean",
        }    
    
    expected_output = {
        "pytest_enabled": {
            "description": "Disable PYTEST job",
            "type": "boolean",
            "default": False
        }
    }

    captured_output = io.StringIO()
    sys.stdout = captured_output

    # Appelle la fonction qui utilise print
    _migrate_var(input_var, "")

    # Récupère la sortie capturée
    captured_output = captured_output.getvalue().strip()

    # Restaure la sortie standard
    sys.stdout = sys.__stdout__

    # Effectue l'assertion
    assert yaml.safe_load(captured_output) == expected_output

def test_migrate_var_multiline_comment(self):
    input_var = {
        "name": "AWS_ENVIRONMENT_URL",
        "type": "url",
        "description": "The default environments url _(only define for static environment URLs declaration)_\n\n_supports late variable expansion"
    }
    
    expected_output = {
        "environment_url": {
            "type": "url",
            "description": "The default environments url _(only define for static environment URLs declaration)_\n\n_supports late variable expansion"
        }
    }

    captured_output = io.StringIO()
    sys.stdout = captured_output

    # Appelle la fonction qui utilise print
    _migrate_var(input_var, "")

    # Récupère la sortie capturée
    captured_output = captured_output.getvalue().strip()

    # Restaure la sortie standard
    sys.stdout = sys.__stdout__

    # Effectue l'assertion
    assert yaml.safe_load(captured_output) == expected_output        